package io.gitlab.aucampia.trx

import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithName
import java.net.URI

@ConfigMapping(prefix = "greeting")
interface GreetingConfig {

  @WithName("message") fun message(): String?

  @WithName("pubsub-topic-uri") fun pubsubTopicURI(): URI
}
