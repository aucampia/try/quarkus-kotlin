package io.gitlab.aucampia.trx

import com.google.cloud.pubsub.v1.Publisher
import com.google.protobuf.ByteString
import com.google.pubsub.v1.PubsubMessage
import com.google.pubsub.v1.TopicName
import com.spotify.futures.ApiFuturesExtra
import io.quarkiverse.loggingjson.providers.KeyValueStructuredArgument.kv
import java.io.Closeable
import java.net.URI
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.future.await
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.slf4j.LoggerFactory

@Path("/hello")
class GreetingResource(
    @ConfigProperty(name = "greeting.pubsub-topic-uri") val pubsubTopicURI: URI
) : Closeable {

  private val logger = LoggerFactory.getLogger(javaClass)
  private val publisher: Publisher

  init {
    logger.info("init ...", kv("pubsubTopicURI", pubsubTopicURI))
    val parts = pubsubTopicURI.path.split("/")
    val projectName = parts[3]
    val topicName = parts[5]
    logger.info("configuring topic", kv("projectName", projectName), kv("topicName", topicName))
    val fqTopicName = TopicName.of(projectName, topicName)
    publisher = Publisher.newBuilder(fqTopicName).build()
  }

  @GET @Produces(MediaType.TEXT_PLAIN) fun hello() = "Hello from RESTEasy Reactive"

  @POST
  @Consumes("application/json")
  suspend fun publish(body: String): Response = coroutineScope {
    try {
      logger.info("got event ...", kv("body", body))
      val data = ByteString.copyFromUtf8(body)
      val message = PubsubMessage.newBuilder().setData(data).build()
      val future = publisher.publish(message)
      ApiFuturesExtra.toCompletableFuture(future).await()
      logger.info("event published ...")
      Response.ok().build()
    } catch (error: Exception) {
      logger.error("caught", error)
      Response.serverError().build()
    }
  }

  override fun close() {
    logger.info("shutting down publisher ...")
    publisher.shutdown()
  }
}
