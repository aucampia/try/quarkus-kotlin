plugins {
  kotlin("jvm") version "1.6.21"
  kotlin("plugin.allopen") version "1.6.21"
  id("io.quarkus")
  id("com.diffplug.spotless") version "6.6.1"
}

repositories {
  mavenCentral()
  mavenLocal()
}

spotless {
  kotlin { ktfmt() }
  kotlinGradle {
    target("*.gradle.kts")
    ktfmt()
  }
}

val quarkusPlatformGroupId: String by project
val quarkusPlatformArtifactId: String by project
val quarkusPlatformVersion: String by project

dependencies {
  implementation(
      enforcedPlatform(
          "${quarkusPlatformGroupId}:${quarkusPlatformArtifactId}:${quarkusPlatformVersion}"))
  implementation("io.quarkus:quarkus-config-yaml")
  implementation("io.quarkiverse.loggingjson:quarkus-logging-json:1.1.1")
  implementation("io.quarkus:quarkus-jackson")
  implementation("io.quarkus:quarkus-resteasy-reactive-jackson")
  implementation("io.quarkus:quarkus-arc")
  implementation("io.quarkus:quarkus-kotlin")
  implementation("io.quarkus:quarkus-resteasy-reactive")
  implementation("io.quarkus:quarkus-micrometer")
  implementation("io.quarkus:quarkus-oidc")
  implementation("io.quarkus:quarkus-rest-client-jackson")
  implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
  testImplementation("io.quarkus:quarkus-junit5")
  testImplementation("io.rest-assured:rest-assured")
  testImplementation(kotlin("test"))
  implementation("com.spotify:futures-extra:4.3.1")
  implementation(platform("com.google.cloud:libraries-bom:25.4.0"))
  implementation("com.google.cloud:google-cloud-pubsub")
}

group = "io.gitlab.aucampia.trx"

version = "1.0.0-SNAPSHOT"

java {
  sourceCompatibility = JavaVersion.VERSION_17
  targetCompatibility = JavaVersion.VERSION_17
}

allOpen {
  annotation("javax.ws.rs.Path")
  annotation("javax.enterprise.context.ApplicationScoped")
  annotation("io.quarkus.test.junit.QuarkusTest")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
  kotlinOptions.jvmTarget = JavaVersion.VERSION_17.toString()
  kotlinOptions.javaParameters = true
}
